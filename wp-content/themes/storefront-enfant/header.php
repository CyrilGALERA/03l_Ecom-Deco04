<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>

	<?php do_action('storefront_before_site'); ?>

	<div id="page" class="hfeed site">
		<?php do_action('storefront_before_header'); ?>

		<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">

			<!-- ANNULER -->
			<!-- < ?php
			/**  
			 * Functions hooked into storefront_header action
			 *
			 * @hooked storefront_header_container                 - 0
			 * @hooked storefront_skip_links                       - 5
			 * @hooked storefront_social_icons                     - 10
			 * @hooked storefront_site_branding                    - 20
			 * @hooked storefront_secondary_navigation             - 30
			 * @hooked storefront_product_search                   - 40
			 * @hooked storefront_header_container_close           - 41
			 * @hooked storefront_primary_navigation_wrapper       - 42
			 * @hooked storefront_primary_navigation               - 50
			 * @hooked storefront_header_cart                      - 60
			 * @hooked storefront_primary_navigation_wrapper_close - 68
			 */
			do_action('storefront_header');
			?> 
			-->

			<!-- REMPLACER PAR -->

			<div class="col-full"> <a class="skip-link screen-reader-text" href="#site-navigation">Aller à la navigation</a>
				<a class="skip-link screen-reader-text" href="#content">Aller au contenu</a>
				<div class="site-branding">
					<h1 class="logo"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/" class="custom-logo-link" rel="home" aria-current="page"><img width="280" height="137" src="http://localhost/PHP_2022/03l_Ecom-Deco04/wp-content/uploads/2022/03/Logo_E-Deco.png" class="custom-logo" alt="Ecom-Deco04" /></a></h1>
				</div>
				<nav class="secondary-navigation" role="navigation" aria-label="Navigation secondaire">
					<div class="menu-menu01_site-container">
						<ul id="menu-menu01_site" class="menu">
							<li id="menu-item-321" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-321"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04" aria-current="page">Accueil</a></li>
							<li id="menu-item-323" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-323"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/boutique/">Boutique</a>
								<ul class="sub-menu">
									<li id="menu-item-340" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-340"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/tableau/">Tableau</a></li>
									<li id="menu-item-329" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-329"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/statue/">Statue</a></li>
								</ul>
							</li>
							<li id="menu-item-330" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-330"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/infos-contact/">Infos &#8211; Contact</a></li>
							<li id="menu-item-332" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-332"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/mon-compte/">Mon compte</a></li>
							<li id="menu-item-322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/panier/">Panier</a></li>
						</ul>
					</div>
				</nav><!-- #site-navigation -->
				<div class="site-search">
					<div class="widget woocommerce widget_product_search">
						<form role="search" method="get" class="woocommerce-product-search" action="http://localhost/PHP_2022/03l_Ecom-Deco04/">
							<label class="screen-reader-text" for="woocommerce-product-search-field-0">Recherche pour :</label>
							<input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Recherche de produits&hellip;" value="" name="s" />
							<button type="submit" value="Recherche">Recherche</button>
							<input type="hidden" name="post_type" value="product" />
						</form>
					</div>
				</div>
			</div>
			<div class="storefront-primary-navigation">
				<div class="col-full">
					<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Navigation principale">
						<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span>Menu</span></button>
						
						<p> Boutique </p>
						<div class="primary-navigation">
							<ul id="menu-menu02_produits" class="menu">
								<li id="menu-item-342" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-342"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/tableau/">Tableau</a>
									<ul class="sub-menu">
										<li id="menu-item-343" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-343"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/tableau/tableau-abstrait/">Tableau abstrait</a></li>
										<li id="menu-item-344" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-344"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/tableau/tableau-paysage/">Tableau paysage</a></li>
									</ul>
								</li>
								<li id="menu-item-345" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-345"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/statue/">Statue</a>
									<ul class="sub-menu">
										<li id="menu-item-346" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-346"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/statue/statue-egyptienne/">Statue Egyptienne</a></li>
										<li id="menu-item-347" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-347"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/categorie-produit/decoration/statue/statue-greco-romaine/">Statue Gréco Romaine</a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div class="menu">
							<ul>
								<li class="current_page_item"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/">Accueil</a></li>
								<li class="page_item page-item-11"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/boutique/">Boutique</a></li>
								<li class="page_item page-item-58"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/condition-generale-de-ventes/">Condition Générale de Ventes</a></li>
								<li class="page_item page-item-170"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/infos-contact/">Infos &#8211; Contact</a></li>
								<li class="page_item page-item-14"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/mon-compte/">Mon compte</a></li>
								<li class="page_item page-item-2"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/page-d-exemple/">Page d’exemple</a></li>
								<li class="page_item page-item-12"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/panier/">Panier</a></li>
								<li class="page_item page-item-15"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/remboursements_retours/">Politique en matière de remboursements et de retours</a></li>
								<li class="page_item page-item-13"><a href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/commander/">Validation de la commande</a></li>
							</ul>
						</div>
					</nav><!-- #site-navigation -->
					<ul id="site-header-cart" class="site-header-cart menu">
						<li class="">
							<a class="cart-contents" href="http://localhost/PHP_2022/03l_Ecom-Deco04/index.php/panier/" title="Afficher votre panier">
								<span class="woocommerce-Price-amount amount">0,00&nbsp;<span class="woocommerce-Price-currencySymbol">&euro;</span></span> <span class="count">0 article</span>
							</a>
						</li>
						<li>
							<div class="widget woocommerce widget_shopping_cart">
								<div class="widget_shopping_cart_content"></div>
							</div>
						</li>
					</ul>
				</div>
			</div>

		</header><!-- #masthead -->

		<?php
		/**
		 * Functions hooked in to storefront_before_content
		 *
		 * @hooked storefront_header_widget_region - 10
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action('storefront_before_content');
		?>

		<div id="content" class="site-content" tabindex="-1">
			<div class="col-full">

				<?php
				do_action('storefront_content_top');
